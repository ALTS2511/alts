"use strict";

// Упражнение 1

for (let a = 2; a <= 20; a++)
  if (a % 2 == 0)
  console.log(a);


  
// Упражнение 2 

function culc_sum() {
  let sum = 0;
  for (let i = 0; i < 3; i++) {
    let value = +prompt("Введите число:", "");
    if (!Number(value)) {
      alert("Ошибка, вы ввели не число");
      return;
    }
    sum += value;
  }
  return alert(sum);
}

culc_sum();


// Упражнение 3 
  let months = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
  ];
  
  function getNameOfMonth(month) {
    return alert(months[month]);
  }
  
  let value = prompt("Впишите порядковый номер месяца в году от 0 до 11", "");
  
  getNameOfMonth(value);
  
  for (let i = 0; i < months.length; i++) {
    if (i === 9) continue;
    console.log(months[i]);
  }



  // Упражнение 4 

      // Что такое IIFE?
      var result = (function () {
        var name = "Barry";
        return name;
        })();
      result; 
   
 