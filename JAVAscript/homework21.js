"use strict"

// Упражнение 1 
const object = {
  //name: 'Alan',
};

/**
 * Check any object
 * @param {object} nameObeject
 * @param {string} key
 * @returns true if object is empty
 */
function isEmpty(nameObeject) {
  for (let key in nameObeject) {
    return false;
  }
  return true;
}

console.log(isEmpty(object));


// Упражнение 2 


// Упражнение 3
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000
  };
  function raiseSalary(perzent) {
    for (let key in salaries) {
      salaries[key] = Math.floor((salaries[key] / 100) * perzent + salaries[key]);
    }
    return salaries;
  }
  console.log(raiseSalary(5));
  
  let sumSalaries = 0;
  for (let key in salaries) {
    sumSalaries += salaries[key];
  }
  console.log(sumSalaries);
  
  

